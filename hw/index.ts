var canvas = document.getElementById('canvas') as HTMLCanvasElement;
var c = canvas.getContext('2d') as CanvasRenderingContext2D;
var Coords = canvas.getBoundingClientRect();
var btn = document.getElementsByTagName('button')[0];
var square = document.getElementsByClassName('result')[0];
var figureSquare = 0;
c.lineCap = 'round';
c.fillStyle = 'rgba(30, 250, 100, 1)';
var points = 0;
const pointsArray = [] as Point[];
const firstPoint = {} as Point;
let x:number;
let y:number;

interface Point {
    x: number,
    y: number
}

var distance = function (p1: Point, p2: Point): number {
    return Math.sqrt(Math.pow((p2.x - p1.x), 2) + Math.pow((p2.y - p1.y), 2));
};
var getSquare = function () {
    var pixelsArr = c.getImageData(0, 0, canvas['width'], canvas['height']).data;
    var square: number = 0;
    for (var i = 0; i < pixelsArr.length; i += 4) {
        if (pixelsArr[i] === 30) {
            square++;
        }
    }
    return square;
};
var showResult = function () {
    square.textContent =
        "Площа " + figureSquare + "px";
};
canvas.addEventListener('click', function (event): void {
    var clickX: number = event.clientX - Coords.x;
    var clickY: number = event.clientY - Coords.y;
    pointsArray.push({ x: clickX, y: clickY });
    points++;
    if (points === 100) {
        alert('Ви перевищили максимум');
    }
    if (!x && !y) {
        firstPoint.x = clickX;
        firstPoint.y = clickY;
        x = firstPoint.x;
        y = firstPoint.y;
    }
    c.beginPath();
    c.moveTo(x, y);
    c.lineTo(clickX, clickY);
    c.stroke();
    c.closePath();
    x = clickX;
    y = clickY;
});
btn.addEventListener('click', function () {
    c.clearRect(0, 0, canvas.width, canvas.height);
  c.moveTo(firstPoint.x, firstPoint.y);

  for (const point of pointsArray) {
    c.lineTo(point.x, point.y);
  }

    c.closePath();
    c.fill();
    c.stroke();
    pointsArray.push(firstPoint);
    figureSquare = getSquare();
    showResult();
})
